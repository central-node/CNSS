'use strict'

const path = require('path');
const exec = require('child_process').exec;
const tap = require('tap');

tap.test('build', (t) => {
  t.plan(1);

  t.test('should not generate error', (t) => {
    exec('npm run build', (err) => {
      t.equal(err, null);
      t.end();
    });
  });
});
