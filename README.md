CNSS
====

Central Node CSS framework. [Online preview](https://central-node.gitlab.io/CNSS)

## Features:

- vendor prefixes
- variables
- imports
- colors

## Build:

    npm run build


## Preview:

    npm run serve
    
## Watch (developer mode):

    npm run watch
    

-----

> [central-node](http://gitlab.com/central-node)
